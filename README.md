# Tolarian Copyshop

A desktop application to print mtg proxy decks.

The Tolarian Copyshop is unofficial Fan Content permitted under the Fan Content Policy. 
Not approved/endorsed by Wizards. Portions of the materials used are property of Wizards of the Coast. ©Wizards of the Coast LLC.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
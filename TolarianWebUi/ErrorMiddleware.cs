﻿using Microsoft.AspNetCore.Http;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TolarianWebUi
{
    public class ErrorMiddleware
    {
        private readonly RequestDelegate _next;

        public ErrorMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next.Invoke(context);
            }
            catch(Exception ex)
            {
                await WriteErrorResponse(context, ex, 500);
            }
        }

        private async Task WriteErrorResponse(HttpContext context, Exception ex, int statusCode)
        {
            StringBuilder responseBuilder = new StringBuilder();
            responseBuilder.AppendLine("An exception occured while executing the request.");
            responseBuilder.AppendLine("Exception:");
            responseBuilder.AppendLine(ex.ToString());


            var content = new StringContent(responseBuilder.ToString());
            context.Response.ContentType = content.Headers.ContentType.ToString();
            context.Response.ContentLength = content.Headers.ContentLength;
            context.Response.StatusCode = statusCode;

            await content.CopyToAsync(context.Response.Body);
        }
    }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule} from './shared/module/shared/shared.module';
import { DeckManagerComponent } from './deck-manager/deck-manager.component'

@NgModule({
  declarations: [
    AppComponent,
    DeckManagerComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,   
    AppRoutingModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

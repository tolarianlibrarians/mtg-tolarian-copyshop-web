import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-deck-manager',
  templateUrl: './deck-manager.component.html',
  styleUrls: ['./deck-manager.component.css']
})
export class DeckManagerComponent implements OnInit {
  
  constructor(private appComponent : AppComponent) { 
    this.appComponent.pageTitle = "Deck Manager";
  }

  ngOnInit(): void {
  }

}

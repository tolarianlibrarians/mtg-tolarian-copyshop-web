import { Guid } from 'guid-typescript';

export interface CardSearchResponse {
    resultsCount: string;
    results: SearchCard[];
}

export interface FullCardResponse {
    card: FullCard;
}

export interface FullCard {
    cardCount: number;
    printId: Guid;
    legalities: string;
    setCode: string;
    manaCostLine: string;
    cardFaces: CardFace;
    relatedCards: RelatedCard;
    isTransformable: boolean;
    formattedCardName: string;
    convertedManaCost: number;
    colorIdentity: string;
    colors: string;
    producedMana: string;
}

export interface CardFace {
    cardTypes: string;
    primaryCardType: string;
    name: string;
    largeImage: string;
    smallImage: string;
    croppedImage: string;
    text: string;
    colors: string;
}

export interface RelatedCard {
    id: Guid;
    type: string;
}

export interface SearchCard {
    printid: Guid;
    name: string;
    primaryCardType: string;
    image: string;
    powerToughness: string;
}
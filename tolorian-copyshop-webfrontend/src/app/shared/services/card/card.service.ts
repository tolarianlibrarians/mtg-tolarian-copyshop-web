import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Guid } from 'guid-typescript';

import { CardSearchResponse, FullCardResponse } from './card.model';

@Injectable({
  providedIn: 'root'
})
export class CardService {

  private url = 'https://localhost:44354/api/card';

  constructor(private readonly http: HttpClient) { }

  getSearchResults(query: string, maxCountOfItems: number) {
    return this.http.get<CardSearchResponse>(`${this.url}/search?query=${query}&maxCountOfItems=${maxCountOfItems}`);
  }

  getCard(printId: Guid) {
    return this.http.get<FullCardResponse>(`${this.url}/${printId}`);
  }
}

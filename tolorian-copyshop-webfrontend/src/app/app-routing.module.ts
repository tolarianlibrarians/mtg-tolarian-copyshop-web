import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeckManagerComponent } from './deck-manager/deck-manager.component';

const AppRoutes: Routes = [
  { path: '', redirectTo: 'deck-manager', pathMatch: 'full' },
  { path: 'deck-manager', component: DeckManagerComponent },
  {
    path: '**',
    redirectTo: 'deck-manager'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(AppRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

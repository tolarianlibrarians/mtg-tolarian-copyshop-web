﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using Moq;
using Tolarian.Copyshop.Business.Interfaces;
using Tolarian.Copyshop.Business.UseCaseInteractors;
using Tolarian.Copyshop.Business.Models.SfCardInfo;
using System;
using Tolarian.Copyshop.Business.DbRequestModels;
using System.Threading.Tasks;

namespace Tests.InteractorTests
{
    [TestClass]
    public class CardInteractorTests
    {
        MockRepository _rep;
        Mock<IScryfallCardDataProvider> _providerMock;

        [TestInitialize]
        public void Initialize()
        {
            _rep = new MockRepository(MockBehavior.Strict);
            _providerMock = _rep.Create<IScryfallCardDataProvider>();
        }

        [TestCleanup]
        public void Cleanup()
        {
            _rep.VerifyAll();
        }

        [TestMethod]
        public async Task GetCardByPrintId_Test()
        {
            SfCard dummy = TestUtils.GetDummyCard();
            _providerMock.Setup(m => m.GetCardByPrintId(It.Is<Guid>(g => g == dummy.PrintId))).ReturnsAsync(dummy);
            CardInteractor unitUnderTest = GetInteractor();

            SfCard result = await unitUnderTest.GetCardByPrintId(dummy.PrintId);

            Assert.IsNotNull(result);
            Assert.AreEqual(dummy.PrintId, result.PrintId);
            Assert.AreEqual(dummy.CardId, result.CardId);
            Assert.AreEqual(dummy.Name, result.Name);
        }

        [TestMethod]
        public async Task GetCardsBySearchQuery_Test()
        {
            var dummyList = TestUtils.GetDummyCardCollection();
            _providerMock.Setup(m => m.GetCardNamesByAutoCompleteQuery(It.IsAny<string>())).ReturnsAsync(
                new SfCatalog { ObjectCount = dummyList.Data.Length, Data = new string[] { "dummyName", "dummyName", "dummyName", "dummyName", "dummyName", } });
            _providerMock.Setup(m => m.GetCardCollectionByIdentifiers(It.IsAny<List<GetCardCollectionRequest>>())).ReturnsAsync(dummyList);
            int maxCountOfItems = 3;
            CardInteractor unitUnderTest = GetInteractor();

            var result = await unitUnderTest.GetCardsBySearchQuery("aaaa", maxCountOfItems);

            Assert.IsNotNull(result);
            Assert.AreEqual(maxCountOfItems, result.Item1.Count);
            Assert.AreEqual(dummyList.Data.Length.ToString(), result.Item2);
        }
        
        [TestMethod]
        public async Task GetTokensBySearchQuery_Test()
        {
            var dummyList = new List<SfCard> { TestUtils.GetDummyCard(), TestUtils.GetDummyCard() };

            _providerMock.Setup(m => m.GetTokensByQuery(It.IsAny<string>())).ReturnsAsync(dummyList);
            CardInteractor unitUnderTest = GetInteractor();

            var response = await unitUnderTest.GetTokensByQuery("aaaa");

            Assert.IsNotNull(response);
            Assert.AreEqual(response.Item1, dummyList);
            Assert.AreEqual(response.Item2, dummyList.Count.ToString());
        }

        [TestMethod]
        public async Task GetPrintsOfCard_Test()
        {
            var dummyList = TestUtils.GetDummyCardList();
            _providerMock.Setup(m => m.GetPrintsOfCard(It.IsAny<Guid>())).ReturnsAsync(dummyList.Data.ToList());
            CardInteractor unitUnderTest = GetInteractor();

            var result = await unitUnderTest.GetPrintsOfCard(Guid.Empty);

            Assert.IsNotNull(result);
            Assert.AreEqual(dummyList.CardCount, result.Count);
        }

        private CardInteractor GetInteractor()
        {
            return new CardInteractor(_providerMock.Object);
        }
    }
}

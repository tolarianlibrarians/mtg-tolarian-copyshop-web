﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Refit;
using Tolarian.Copyshop.ScryfallDataAccess;
using System.Linq;
using Tolarian.Copyshop.Business.Models.SfCardInfo;
using Tolarian.Copyshop.Business.DbRequestModels;
using System.Threading.Tasks;

namespace Tests.ScryfallDataAccessTests
{
    [TestClass]
    public class CardDataMapperTest
    {
        IScryfallApi service;

        [TestInitialize]
        public void Initialize()
        {
            service = RestService.For<IScryfallApi>("https://api.scryfall.com");
        }

        [TestMethod]
        public async Task GetCardNamesByAutoCompleteQuery_Test()
        {
            //Arrange
            const string query = "Toothy, Imagina";
            const string expectedCardName = "Toothy, Imaginary Friend";
            ScryfallDataProvider mapper = GetMapper();

            //Act
            SfCatalog result = await mapper.GetCardNamesByAutoCompleteQuery(query);

            //Assert
            Assert.IsTrue(result.ObjectCount == 1);
            Assert.AreEqual(expectedCardName, result.Data[0]);
        }

        [TestMethod]
        public async Task GetCardById_DoubleFaced_Test()
        {
            //Arrange
            Guid id = new Guid("b3b87bfc-f97f-4734-94f6-e3e2f335fc4d");
            ScryfallDataProvider mapper = GetMapper();

            //Act
            SfCard result = await mapper.GetCardByPrintId(id);

            //Assert
            Assert.IsTrue(result != null);
            Assert.AreEqual("Growing Rites of Itlimoc // Itlimoc, Cradle of the Sun", result.Name);
            Assert.AreEqual(2, result.CardFaces.Count);
        }

        [TestMethod]
        public async Task GetCardById_DualCard_Test()
        {
            //Arrange
            Guid id = new Guid("e9d5aee0-5963-41db-a22b-cfea40a967a3");
            ScryfallDataProvider mapper = GetMapper();

            //Act
            SfCard result = await mapper.GetCardByPrintId(id);

            //Assert
            Assert.IsTrue(result != null);
            Assert.AreEqual("Dusk // Dawn", result.Name);
            Assert.IsTrue(result.CardFaces.All(c => c.ImageUris == null));
        }
        
        [TestMethod]
        public async Task GetCardByExactName_RelatedCards_Test()
        {
            //Arrange
            Guid id = new Guid("ebdf2f50-f69a-47c4-a75f-ff55781bb0c8");
            ScryfallDataProvider mapper = GetMapper();

            //Act
            SfCard result = await mapper.GetCardByPrintId(id);

            //Assert
            Assert.IsTrue(result != null);
            Assert.AreEqual("Toothy, Imaginary Friend", result.Name);
            Assert.IsNotNull(result.RelatedCards);
        }

        [TestMethod]
        public async Task GetPrintsOfCard_Test()
        {
            Guid dummyOracleGuid = new Guid("900ca697-ad38-4b2b-bc74-2ff7eb6ea951"); //Emrakul, the aeons torn

            ScryfallDataProvider mapper = GetMapper();
            var result = await mapper.GetPrintsOfCard(dummyOracleGuid);

            Assert.IsNotNull(result);
            Assert.AreEqual(8, result.Count);
            Assert.AreEqual(8, result.Count(x => x.CardId == dummyOracleGuid));
        }

        [TestMethod]
        public async Task GetCardsByNameList_Test()
        {
            //Arrange
            List<GetCardCollectionRequest> names = new List<GetCardCollectionRequest>
            {
                new GetCardCollectionRequest { Name = "Sol Ring" },
                new GetCardCollectionRequest { Name = "Dusk // Dawn" }
            };
            ScryfallDataProvider mapper = GetMapper();

            //Act
            SfCardCollection result = await mapper.GetCardCollectionByIdentifiers(names);

            //Assert
            Assert.IsTrue(result != null);
            Assert.AreEqual(2, result.Data.Length);
        }

        [TestMethod]
        public async Task GetTokensByQuery_Test()
        {
            //Arrange
            const string query = "Gobl";
            ScryfallDataProvider mapper = GetMapper();

            //Act
            var result = await mapper.GetTokensByQuery(query);

            //Assert
            Assert.AreEqual(result.Count, 8);
            Assert.IsTrue(!result.Any(c => !c.Name.Contains(query) && !c.TypeLine.Contains("Token")));
        }

        private ScryfallDataProvider GetMapper()
        {
            return new ScryfallDataProvider();
        }
    }
}

﻿using Microsoft.Extensions.DependencyInjection;
using Tolarian.Copyshop.Business.Entities;
using Tolarian.Copyshop.Business.Interfaces;

namespace Tolarian.Copyshop.ScryfallDataAccess
{
    public static class Extensions
    {
        public static IServiceCollection AddDataLayer(this IServiceCollection services)
        {
            services.AddScoped<ISetCodeTranslator, SetCodeTranslator>();
            services.AddScoped<IImportStringParser, ImportStringParser>();
            services.AddScoped<IScryfallCardDataProvider, ScryfallDataProvider>();
            services.AddScoped<ISetDataGateway, SetDataMapper>();
            return services;
        }
    }
}


﻿using Refit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Tolarian.Copyshop.Business.DbRequestModels;
using Tolarian.Copyshop.Business.Interfaces;
using Tolarian.Copyshop.Business.Models.SfCardInfo;

namespace Tolarian.Copyshop.ScryfallDataAccess
{
    public class ScryfallDataProvider : DataMapperBase, IScryfallCardDataProvider
    {
        IScryfallApi _service;
        //Scryfall will return a maximum of 75 Cards per request
        private const int _scryfallApiReturnCountMaximum = 75;
        public ScryfallDataProvider()
        {
            _service = RestService.For<IScryfallApi>(Constants.SCRYFALL_BASE_URI);
        }

        public async Task<SfCard> GetCardByPrintId(Guid printId)
        {
            ApiResponse<SfCard> response = await _service.GetCardByPrintId(printId);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK:
                    return response.Content;
                case HttpStatusCode.NotFound:
                    return null;
                default:
                    HandleUnexpectedStatusCodeForResponse(response);
                    break;
            }

            return null;
        }

        public async Task<List<SfCard>> GetPrintsOfCard(Guid oracleId)
        {
            ApiResponse<SfPaginatedCardList> firstPageResponse = await _service.GetPrintsBySearchQuery(oracleId, 1);

            switch (firstPageResponse.StatusCode)
            {
                case HttpStatusCode.OK:
                    SfPaginatedCardList currentPage = firstPageResponse.Content;
                    var result = currentPage.Data.ToList();

                    if(currentPage.MorePagesAvailable)
                        result.AddRange(await ReadNextPagesOf(currentPage, oracleId));

                    return result;
                case HttpStatusCode.NotFound:
                    return new List<SfCard>();
                default:
                    HandleUnexpectedStatusCodeForResponse(firstPageResponse);
                    break;
            }

            return null;
        }

        private async Task<List<SfCard>> ReadNextPagesOf(SfPaginatedCardList firstPage, Guid oracleId)
        {
            SfPaginatedCardList currentPage = firstPage;
            int currentPageNumber = 1;
            var result = new List<SfCard>();

            while (currentPage.MorePagesAvailable)
            {
                currentPageNumber++;
                var nextPageResponse = await _service.GetPrintsBySearchQuery(oracleId, currentPageNumber);

                switch (nextPageResponse.StatusCode)
                {
                    case HttpStatusCode.OK:
                        currentPage = nextPageResponse.Content;
                        result.AddRange(currentPage.Data);
                        break;
                    default:
                        HandleUnexpectedStatusCodeForResponse(nextPageResponse);
                        break;
                }
            }

            return result;
        }

        public async Task<SfCardCollection> GetCardCollectionByIdentifiers(List<GetCardCollectionRequest> request)
        {
            if (request.Count == 0)
            {
                return SfCardCollection.GetEmpty();
            }

            //List needs to be chunked into lists of max 75 items because SF will return a maximum of 75 cards
            List<List<GetCardCollectionRequest>> chunkedRequests = ChunkListBySize(request, _scryfallApiReturnCountMaximum);

            var containers = chunkedRequests.Select(cr => new SfIdentifierContainer
            {
                Identifiers = cr.Select(r => new SfIdentifier { Name = r.Name, SetCode = r.SetCode, Id = r.Id }).ToList()
            });

            List<SfCardCollection> temp = new List<SfCardCollection>();

            foreach(var container in containers)
            {
                temp.Add(await IssueGetCardCollectionRequest(container));

                // Give Scryfall time to breath
                await Task.Delay(100);
            }

            SfCardCollection result = MergeCardCollections(temp);

            return result;
        }

        private async Task<SfCardCollection> IssueGetCardCollectionRequest(SfIdentifierContainer container)
        {
            ApiResponse<SfCardCollection> response = await _service.GetCardsByCollection(container);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK:
                    return response.Content;
                case HttpStatusCode.NotFound:
                    return SfCardCollection.GetEmpty();
                default:
                    HandleUnexpectedStatusCodeForResponse(response);
                    break;
            }

            return null;
        }

        private SfCardCollection MergeCardCollections(IEnumerable<SfCardCollection> response)
        {
            SfCardCollection result = new SfCardCollection();
            result.Data = response.SelectMany(r => r.Data).ToArray();
            result.NotFound = response.SelectMany(r => r.NotFound).ToArray();
            return result;
        }

        private List<List<T>> ChunkListBySize<T>(List<T> source, int chunkSize)
        {
            return source
                .Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / chunkSize)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();
        }

        public async Task<SfCatalog> GetCardNamesByAutoCompleteQuery(string query)
        {
            ApiResponse<SfCatalog> response = await _service.GetCardsByAutoCompleteQuery(query);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK:
                    return response.Content;
                case HttpStatusCode.NotFound:
                    return SfCatalog.GetEmpty();
                default:
                    HandleUnexpectedStatusCodeForResponse(response);
                    break;
            }

            return null;
        }

        public async Task<List<SfCard>> GetTokensByQuery(string query)
        {
            ApiResponse<SfPaginatedCardList> response = await _service.GetTokensByQuery(query);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK:
                    return response.Content.Data.ToList();
                case HttpStatusCode.NotFound:
                    return  new List<SfCard>();
                default:
                    HandleUnexpectedStatusCodeForResponse(response);
                    break;
            }

            return null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tolarian.Copyshop.Business.DbRequestModels;
using Tolarian.Copyshop.Business.Interfaces;
using Tolarian.Copyshop.Business.Models.SfCardInfo;

namespace Tolarian.Copyshop.Business.UseCaseInteractors
{
    public class CardInteractor : ICardDataRequester
    {
        private readonly IScryfallCardDataProvider _gateway;

        public CardInteractor(IScryfallCardDataProvider gateway)
        {
            _gateway = gateway;
        }

        public async Task<SfCard> GetCardByPrintId(Guid printId)
        {
            SfCard result = await _gateway.GetCardByPrintId(printId);
            return result;
        }

        public async Task<(List<SfCard>, string)> GetCardsBySearchQuery(string searchQuery, int maxCountOfItems)
        {
            const int minimumQueryLength = 3;
            if (searchQuery.Length < minimumQueryLength || maxCountOfItems <= 0)
            {
                return (new List<SfCard>(), "0");
            }

            List<string> cardNames = (await _gateway.GetCardNamesByAutoCompleteQuery(searchQuery)).Data.ToList();

            List<SfCard> result = (await _gateway.GetCardCollectionByIdentifiers(
                cardNames.Select(name => new GetCardCollectionRequest { Name = name }).ToList()
                )).Data.ToList();

            return (TruncateListToMaxSize(maxCountOfItems, result), cardNames.Count >= 20 ? "20+" : cardNames.Count.ToString());
        }

        private List<SfCard> TruncateListToMaxSize(int maxCountOfItems, List<SfCard> targetList)
        {
            List<SfCard> resultList = new List<SfCard>(targetList);
            int firstInvalidIndex = maxCountOfItems;

            if (resultList.Count > maxCountOfItems)
                resultList.RemoveRange(firstInvalidIndex, resultList.Count - firstInvalidIndex);

            return resultList;
        }

        public async Task<List<SfCard>> GetPrintsOfCard(Guid cardId)
        {
            return (await _gateway.GetPrintsOfCard(cardId)).OrderByDescending(card => card.ReleaseDate).ToList();
        }

        public async Task<(List<SfCard>, string)> GetTokensByQuery(string searchQuery)
        {
            var result = await _gateway.GetTokensByQuery(searchQuery);
            return (result, result.Count.ToString());
        }

        public async Task<List<SfCard>> GetCardsByIds(List<Guid> cardIds)
        {
            if (!cardIds.Any())
                return new List<SfCard>();

            var result = (await _gateway.GetCardCollectionByIdentifiers(cardIds.Select(tg => new GetCardCollectionRequest { Id = tg }).ToList())).Data
                .ToList();
            return result;
        }
    }
}

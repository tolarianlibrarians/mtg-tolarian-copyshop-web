﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tolarian.Copyshop.Business.Models.SfCardInfo;

namespace Tolarian.Copyshop.Business.Interfaces
{
    public interface ICardDataRequester
    {
        Task<(List<SfCard> Cards, string AmountFound)> GetCardsBySearchQuery(string searchQuery, int maxCountOfItems);
        Task<SfCard> GetCardByPrintId(Guid printId);
        Task<List<SfCard>> GetPrintsOfCard(Guid cardId);
        Task<(List<SfCard>, string)> GetTokensByQuery(string searchQuery);
        Task<List<SfCard>> GetCardsByIds(List<Guid> tokenGuids);
    }
}

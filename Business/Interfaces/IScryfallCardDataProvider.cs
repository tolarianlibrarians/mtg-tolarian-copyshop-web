﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tolarian.Copyshop.Business.DbRequestModels;
using Tolarian.Copyshop.Business.Models.SfCardInfo;

namespace Tolarian.Copyshop.Business.Interfaces
{
    public interface IScryfallCardDataProvider
    {
        Task<SfCatalog> GetCardNamesByAutoCompleteQuery(string query);
        Task<SfCard> GetCardByPrintId(Guid printId);
        Task<SfCardCollection> GetCardCollectionByIdentifiers(List<GetCardCollectionRequest> cardNames);
        Task<List<SfCard>> GetPrintsOfCard(Guid oracleId);
        Task<List<SfCard>> GetTokensByQuery(string query);
    }
}

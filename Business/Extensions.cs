﻿using Microsoft.Extensions.DependencyInjection;
using Tolarian.Copyshop.Business.Interfaces;
using Tolarian.Copyshop.Business.UseCaseInteractors;

namespace Tolarian.Copyshop.Business
{
    public static class Extensions
    {
        public static IServiceCollection AddBusinessLayer(this IServiceCollection services)
        {
            services.AddScoped<ICardDataRequester, CardInteractor>();
            services.AddScoped<IPrintRequester ,PrintInteractor>();
            services.AddScoped<IDeckExporter ,DeckExporter>();
            services.AddScoped<IDeckInfoInteractor ,DeckInfoInteractor>();
            services.AddScoped<IDeckImportInteractor ,DeckImportInteractor>();
            services.AddScoped<ISaveAndLoadInteractor, SaveAndLoadInteractor>();
            return services;
        }
    }
}

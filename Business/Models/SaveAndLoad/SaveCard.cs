﻿using System;

namespace Tolarian.Copyshop.Business.Models.SaveAndLoad
{
    public class SaveCard
    {
        public Guid PrintId { get; set; }
        public int CardCount { get; set; }
    }
}

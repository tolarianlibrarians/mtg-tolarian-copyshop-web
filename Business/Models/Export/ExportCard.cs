﻿namespace Tolarian.Copyshop.Business.Models.Export
{
    public class ExportCard
    {
        public int CardCount { get; set; }
        public string Name { get; set; }
        public string SetCode { get; set; }
    }
}

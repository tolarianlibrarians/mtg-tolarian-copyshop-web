﻿namespace Tolarian.Copyshop.Business.Models.Enums
{
    public enum MtgPlayModes
    {
        Standard,
        Future,
        Historic,
        Pioneer,
        Modern,
        Legacy,
        Pauper,
        Vintage,
        Penny,
        Commander,
        Brawl,
        Duel,
        Oldschool
    }

}

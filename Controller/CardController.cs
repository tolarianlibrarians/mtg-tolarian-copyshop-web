﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Tolarian.Copyshop.Business.Interfaces;
using Tolarian.Copyshop.Business.Models.SfCardInfo;
using Tolarian.Copyshop.Controller.Mappers;
using Tolarian.Copyshop.Controller.ResponseObjects;

namespace Tolarian.Copyshop.Controller
{
    [ApiController]
    [Route("api/card")]
    public class CardController : ControllerBase
    {
        private readonly ICardDataRequester _requester;
        private readonly IDeckImportInteractor _importInteractor;

        public CardController(ICardDataRequester requester, IDeckImportInteractor importInteractor)
        {
            _requester = requester;
            _importInteractor = importInteractor;
        }

        /// <summary>
        /// Gets the information for one Card by ID. This returns a List because the target card may be multifaced.
        /// </summary>
        [HttpGet("{printId}")]
        public async Task<ActionResult<FullCardResponse>> GetCardByPrintId([FromRoute] Guid printId)
        {
            FullCardResponse response = new FullCardResponse();

            SfCard card = await _requester.GetCardByPrintId(printId);

            if(card == null)
            {
                return NotFound();
            }

            response.Card = CardMapper.MapToCardDto(card);

            return response;
        }

        [HttpGet("search")]
        public async Task<ActionResult<CardSearchResponse>> GetSearchResults([FromQuery][Required] string query, [FromQuery][Required] int maxCountOfItems)
        {
            (List<SfCard> Cards, string amountFound) businessResponse = await _requester.GetCardsBySearchQuery(query, maxCountOfItems);
            var response = CardMapper.MapToSearchResultDto(businessResponse.Cards, businessResponse.amountFound);

            return response;
        }

        [HttpGet("artwork/{cardId}")]
        public async Task<ActionResult<CardArtworkResponse>> GetArtworksOfCard([FromRoute] Guid cardId)
        {
            CardArtworkResponse response = new CardArtworkResponse();

            List<ArtworkCard> artworks = CardMapper.MapToArtworkDto(await _requester.GetPrintsOfCard(cardId));
            response.Artworks = artworks;

            return response;
        }

        [HttpPost("import/string")]
        public async Task<ActionResult<CardImportResponse>> GetCardsByImportString([FromBody] string importString)
        {
            CardImportResponse response = new CardImportResponse();

            List<string> lines = CardMapper.PrepareImportStringForBusiness(importString);
            (List<SfCard> Cards, string NotFound) = _importInteractor.GetCardsForImport(lines);
            response.Cards = CardMapper.MapToCardDto(Cards);
            response.NotFound = NotFound;

            return response;
        }

        [HttpGet("import/url")]
        public async Task<ActionResult<CardImportResponse>> GetCardsFromUri([FromQuery][Required] string deckUrl)
        {
            CardImportResponse response = new CardImportResponse();

            Uri uri = new Uri(deckUrl);

            if (uri.Host.IndexOf("tappedout", StringComparison.InvariantCultureIgnoreCase) == -1)
            {
                return BadRequest("Currently, we only support importing decks from tappedout.net.");
            }

            (List<SfCard> Cards, string NotFound) = _importInteractor.ImportFromTappedOut(uri);

            response.Cards = CardMapper.MapToCardDto(Cards);
            response.NotFound = NotFound;

            return response;
        }

        [HttpGet("token/search")]
        public async Task<ActionResult<CardSearchResponse>> GetTokenSearchResults([FromQuery][Required] string query)
        {
            (List<SfCard> Cards, string amountFound) businessResponse = await _requester.GetTokensByQuery(query);
            var response = CardMapper.MapToSearchResultDto(businessResponse.Cards, businessResponse.amountFound);

            return response;
        }

        [HttpPost("tokens/import")]
        public async Task<ActionResult<AddTokensToDeckResponse>> AddTokensToDeck([FromBody] List<FullCard> deckCards, [FromQuery][Required] bool overwriteTokens)
        {
            AddTokensToDeckResponse response = new AddTokensToDeckResponse();

            if (overwriteTokens)
            {
                deckCards.RemoveAll(c => c.CardFaces.Any(cf => cf.PrimaryCardType == ResponseObjects.Enums.CardType.Token));
            }

            List<SfCard> businessResponse = await _requester.GetCardsByIds(CardMapper.GetTokenGuidsOfDeck(deckCards));
            deckCards.AddRange(CardMapper.MapToCardDto(businessResponse));
            response.Deck = deckCards;

            return response;
        }
    }
}

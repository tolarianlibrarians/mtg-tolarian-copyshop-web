﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Tolarian.Copyshop.Business.Interfaces;
using Tolarian.Copyshop.Business.Models.Export;
using Tolarian.Copyshop.Controller.ResponseObjects;

namespace Tolarian.Copyshop.Controller
{
    [ApiController]
    [Route("export")]
    public class ExportController : ControllerBase
    {
        private readonly IDeckExporter _deckExporter;

        public ExportController(IDeckExporter deckExporter)
        {
            this._deckExporter = deckExporter;
        }

        [HttpPost]
        public string ExportDeck([FromBody] List<FullCard> deck)
        {
            return this._deckExporter.ExportDeck(deck.Select(card => new ExportCard
            {
                CardCount = card.CardCount,
                Name = card.CardFaces.First().Name,
                SetCode = card.SetCode
            }).ToList()
            );
        }
    }
}

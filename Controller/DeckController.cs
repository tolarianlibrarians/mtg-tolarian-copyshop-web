﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Tolarian.Copyshop.Business.Interfaces;
using Tolarian.Copyshop.Business.Models.DeckInfo;
using Tolarian.Copyshop.Controller;
using Tolarian.Copyshop.Controller.Mappers;
using Tolarian.Copyshop.Controller.ResponseObjects;
using Tolarian.Copyshop.Controller.ResponseObjects.Enums;

namespace Tolarian.Copyshop.Controller
{
    [ApiController]
    [Route("api/deck")]
    public class DeckController : ControllerBase
    {
        private readonly IDeckInfoInteractor _deckInfoInteractor;
        private readonly ISaveAndLoadInteractor _saveAndLoadInteractor;

        public DeckController(IDeckInfoInteractor deckInfoInteractor, ISaveAndLoadInteractor saveAndLoadInteractor)
        {
            _deckInfoInteractor = deckInfoInteractor;
            _saveAndLoadInteractor = saveAndLoadInteractor;
        }

        //public List<FullCard> LoadDeckFromFile(string fileName)
        //{
        //    var response = new List<FullCard>();
        //    var businessResponse = _saveAndLoadInteractor.LoadDeck(fileName);
        //    response = CardMapper.MapToCardDto(businessResponse);

        //    return response;
        //}

        //public bool SaveDeckToFile(string fileName, List<FullCard> deckCards)
        //{
        //    _saveAndLoadInteractor.SaveDeck(SaveAndLoadMapper.ConvertToBusiness(deckCards), fileName);
        //    return true;
        //}

        [HttpPost("card-count")]
        public int GetTotalCardCountOfDeck([FromBody] List<FullCard> deckCards)
        {
            List<DeckInfoCard> businessModel = DeckMapper.MapDeckDtoToBusiness(deckCards);
            return _deckInfoInteractor.GetTotalCardCountOfDeck(businessModel);
        }

        [HttpPost("statistics")]
        public GetDeckStatisticsResponse GetDeckStatistics([FromBody] List<FullCard> deckCards)
        {
            if (deckCards == null)
                return GetDeckStatisticsResponse.Empty();

            var businessModel = DeckMapper.MapDeckDtoToBusiness(deckCards);

            var result = new GetDeckStatisticsResponse
            {
                CardTypeCounts = _deckInfoInteractor.GetCardTypeCounts(businessModel).ToDictionary(x => (CardType)((int)x.Key), x => x.Value),
                ColorCardsCounts = _deckInfoInteractor.GetColorCardCounts(businessModel).ToDictionary(x => (MtgColor)((int)x.Key), x => x.Value),
                ColorSymbolCounts = _deckInfoInteractor.GetColorSymbolCounts(businessModel).ToDictionary(x => (MtgColor)((int)x.Key), x => x.Value),
                ManaSourcesCounts = _deckInfoInteractor.GetManaSourcesCounts(businessModel).ToDictionary(x => (MtgColor)((int)x.Key), x => x.Value),
                ManaCurveCreatures = _deckInfoInteractor.GetCreatureManaCurve(businessModel),
                ManaCurveNonCreatures = _deckInfoInteractor.GetNonCreatureManaCurve(businessModel),
                TotalCards = _deckInfoInteractor.GetTotalCardCountOfDeck(businessModel),
                AverageCmc = _deckInfoInteractor.GetAverageCmc(businessModel),
                CreatureCount = _deckInfoInteractor.GetCreatureCount(businessModel),
                NonCreatureCount = _deckInfoInteractor.GetNonCreatureCount(businessModel),
            };

            return result;
        }
    }
}

﻿namespace Tolarian.Copyshop.Controller.ResponseObjects
{
    public class FullCardResponse
    {
        public FullCard Card { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace Tolarian.Copyshop.Controller.ResponseObjects
{
    public class CardImportResponse
    {
        public string NotFound { get; set; }
        public List<FullCard> Cards { get; set; } = new List<FullCard>();
    }
}

﻿using System.Collections.Generic;

namespace Tolarian.Copyshop.Controller.ResponseObjects
{
    public class AddTokensToDeckResponse
    {
        public List<FullCard> Deck { get; set; }
    }
}

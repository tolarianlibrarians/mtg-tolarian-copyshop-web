﻿using System;
using System.Collections.Generic;
using Tolarian.Copyshop.Controller.ResponseObjects.Enums;

namespace Tolarian.Copyshop.Controller.ResponseObjects
{
    public class FullCard
    {
        public int CardCount { get; set; }
        public Guid CardId { get; set; }
        public Guid PrintId { get; set; }
        public Dictionary<string, string> Legalities { get; set; }
        public string SetCode { get; set; }
        public string ManaCostLine { get; set; }
        public ICollection<CardFace> CardFaces { get; set; }
        public ICollection<RelatedCard> RelatedCards { get; set; }
        public bool IsTransformable { get; set; }
        public string FormattedCardName { get; set; }
        public float ConvertedManaCost { get; set; }
        public List<MtgColor> ColorIdentity { get; set; }
        public List<MtgColor> Colors { get; set; }
        public List<MtgColor> ProducedMana { get; set; }
    }
}
